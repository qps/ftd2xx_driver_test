/*
 Name:		Teensy36_transceiver.ino
 Created:	29/04/2020 9:21:03
 Author:	guzma
*/

#include <TSPISlave.h>
#include <i2c_t3.h>

// Function prototypes
void receiveEvent(size_t count);
void requestEvent(void);

// Memory
#define MEM_LEN 256
char databuf[MEM_LEN];
volatile uint8_t received;

const uint8_t i2c_addr = 0x0D;

TSPISlave spi_slave = TSPISlave(SPI, 12, 11, 13, 10, 16);

bool firstpush = true;
volatile bool spi_recv_flag = false;
volatile uint16_t spi_buffer;

uint16_t answer = 0xE0E0;

void setup() {
    Serial1.begin(9600, SERIAL_8N1);
    spi_slave.onReceive(spi_slave_ISR);
    Wire.begin(I2C_SLAVE, 0x0D, I2C_PINS_18_19, I2C_PULLUP_EXT, 10000);

    // Data init
    received = 0;
    memset(databuf, 0, sizeof(databuf));

    // register events
    Wire.onReceive(receiveEvent);
    Wire.onRequest(requestEvent);
    Serial.begin(115200);

    delay(1000);
    Serial.println("Startup ok");
}

void loop() {
    // print received data - this is done in main loop to keep time spent in I2C ISR to minimum
    if (received)
    {
        Serial.print("Slave recv: ");
        Serial.println(databuf[0], HEX);
        received = 0;
    }
    if (spi_recv_flag) {
        spi_recv_flag = false;
        Serial.print("SPI RX isr triggered, value: ");
        Serial.print(spi_buffer, HEX);
        Serial.print("\tbin ");
        Serial.println(spi_buffer, BIN);
    }
    if (Serial1.available()) {
        char c = Serial1.read();
        Serial.println(c, BIN);
        Serial1.print(c);
    }
}

//
// handle Rx Event (incoming I2C data)
//
void receiveEvent(size_t count)
{
    Wire.read(databuf, count);  // copy Rx data to databuf
    received = count;           // set received flag to count, this triggers print in main loop
}

//
// handle Tx Event (outgoing I2C data)
//
void requestEvent(void)
{
    Wire.write(databuf, MEM_LEN); // fill Tx buffer (send full mem)
}

void spi_slave_ISR() {
    static uint16_t counter = 0;
    while (spi_slave.active()) {
        if (spi_slave.available()) {
            spi_slave.pushr(spi_buffer);
            spi_buffer = spi_slave.popr();
            spi_recv_flag = true;
        }
    }
}

