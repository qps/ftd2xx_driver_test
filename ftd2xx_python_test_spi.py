import sys
import time
import ftd2xx

class ftd2xx_spi(object):
    #Opens the device and initialices the required variables. SPI_Mode change not yet implemented
    def __init__(self, bus_speed, spi_mode = 0, timeouts = 100, n=0):
        self.__MSB_RISING_EDGE_CLOCK_DATA_IN = 0x20
        self.__MSB_FALLING_EDGE_CLOCK_DATA_IN = 0x24
        self.__MSB_RISING_EDGE_CLOCK_DATA_OUT = 0x10
        self.__MSB_FALLING_EDGE_CLOCK_DATA_OUT = 0x11
        self.__MSB_FALLING_EDGE_DATA_OUT_RISING_EDGE_DATA_IN = 0x31
        self.__MOSI = 0b00000010
        self.__SCK = 0b00000001
        self.__CS = 0b00000001

        self.outputBuffer = []
        self.outputBytes = 0

        self.d = ftd2xx.open(n)
        self.d.setTimeouts(timeouts, timeouts)
        self.d.resetDevice()

        #Bus should be idle before setting the MPSSE mode. Otherwise, glitches might occur.
        self.d.setBitMode(self.__CS << 8 + self.__MOSI + self.__SCK, 0x2)
        self.set_bus_idle()
        self.d.setBitMode(self.__CS << 8 + self.__MOSI + self.__SCK, 0x2)
        self.set_clock_speed(bus_speed)

        self.config_mpsse_spi()
        self.sync_mpsse_interface()

    # Set SPI clock rate
    def set_clock_speed(self, hz):
        div = int((60000000 / (hz * 2)) - 1)  # Set SPI clock
        self.d.write(bytes((0x86, div%256, div//256))) 

    # Write output buffer and clear it
    def ft_write_buffer(self):
        self.d.write(bytes(self.outputBuffer))
        self.outputBuffer = []
        self.outputBytes = 0

    def sync_mpsse_interface(self):
        self.outputBuffer = [0xAB]
        self.ft_write_buffer()

    def config_mpsse_spi(self):
        #Disable clock divide by 5, disable adaptive clocking and disable 3 phase data clock
        self.outputBuffer = [0x8A, 0x97, 0x8D]
        self.ft_write_buffer()

        self.outputBuffer = [0x85]
        self.ft_write_buffer()

    # Write a send data command to the output buffer
    def q_write_data(self, cmd, data):
        n = len(data) - 1
        self.outputBuffer += [cmd, n%256, n//256] + list(data)
        self.outputBytes += 3 + len(data)

    def q_read_data(self, cmd, read_bytes):
        n = read_bytes - 1
        self.outputBuffer += [cmd, n%256, n//256]
        self.outputBytes += 3

    # Write a CS enable (active low) command to the output buffer 5 times (5x0.2us times it takes to execute a command = 1us)
    def q_SPI_CSEnable(self):
        for i in range(0, 10):
            self.outputBuffer += [0x82, 0, self.__CS]
            self.outputBytes += 3

    # Write a CS disable (active high) command to the output buffer 5 times (5x0.2us times it takes to execute a command = 1us)
    def q_SPI_CSDisable(self):
        for i in range(0, 10):
            self.outputBuffer += [0x82, self.__CS, self.__CS]
            self.outputBytes += 3

    #Set the data, chip select high and clock low as default state
    def set_bus_idle(self):
        self.d.write(bytes([0x80, 0, self.__MOSI+self.__SCK]))
        self.d.write(bytes([0x82, self.__CS, self.__CS]))

    def write_data(self, data):
        self.q_SPI_CSEnable()
        data = i//256,  i%256
        self.q_write_data(self.__MSB_FALLING_EDGE_CLOCK_DATA_OUT, data)
        self.q_SPI_CSDisable()
        self.ft_write_buffer()

    def writeread_data(self, data, read_bytes = 2):
        self.q_SPI_CSEnable()
        data = i//256,  i%256
        self.q_write_data(self.__MSB_FALLING_EDGE_DATA_OUT_RISING_EDGE_DATA_IN, data)
        self.q_SPI_CSDisable()
        self.ft_write_buffer()
        return self.d.read(2)

if __name__ == "__main__":
    spi_dev = ftd2xx_spi(100000, 0)
    # Set MPSSE mode
    print("FTDI device opened")
    for i in range(0, 128):
        data = i//256,  i%256     #data to write
        #data = [i]
        answer = spi_dev.writeread_data(data)
        time.sleep(0.1)
        print("Counter sent: " + str(hex(i)))
        print("Answer received: " + answer.hex())

        #for i in range(0, 100):
        #    q_SPI_CSEnable()
        #    data = i//256,  i%256     #data to write
        #    q_write_data(dev, MSB_FALLING_EDGE_CLOCK_DATA_OUT, data)
        #    q_SPI_CSDisable()
        #    ft_write_buffer(dev)
        #    time.sleep(0.01)
        #    print("Counter sent" + str(data))
        #    q_SPI_CSEnable()
        #    q_read_data(dev, MSB_FALLING_EDGE_CLOCK_DATA_IN, 2)
        #    q_SPI_CSDisable()
        #    ft_write_buffer(dev)
        #    print("Answer received: " + str(dev.read(2).hex()))
        #    time.sleep(0.01)

    spi_dev.d.close()