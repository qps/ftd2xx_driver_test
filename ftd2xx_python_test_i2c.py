import sys
import time
import ftd2xx

FTDI_TIMEOUT = 100  # Timeout for D2XX read/write (msec)
MSB_RISING_EDGE_CLOCK_BIT_IN = 0x22
MSB_FALLING_EDGE_CLOCK_BIT_OUT = 0x13

MSB_RISING_EDGE_CLOCK_BYTE_IN = 0x20
MSB_FALLING_EDGE_CLOCK_BYTE_IN = 0x24
MSB_RISING_EDGE_CLOCK_BYTE_OUT = 0x10
MSB_FALLING_EDGE_CLOCK_BYTE_OUT = 0x11

MSB_FALLING_EDGE_DATA_OUT_RISING_EDGE_DATA_IN = 0x31

SCL = 0b00000001
SDAO = 0b00000010   #SDA for output
SDAI = 0b00000100   #SDA for input, both are shorted
outputBuffer = []
outputBytes = 0

# Open device for read/write
def ft_open(n=0):
    d = ftd2xx.open(n)
    d.setTimeouts(FTDI_TIMEOUT, FTDI_TIMEOUT)
    return d
 
# Set I2C clock rate
def set_clock_speed(d, hz):
    div = int((60000000 / (hz * 2)) - 1)  # Set SPI clock
    #in I2C, because we are using 3-phase-clocking mode, the clock divider
                                            #should be 2/3rds of the original
    div = int(div * 2 / 3)
    d.write(bytes((0x86, div % 256, div // 256))) 

# Write output buffer and clear it
def ft_write_buffer(dev):
    global outputBuffer
    global outputBytes
    dev.write(bytes(outputBuffer))
    outputBuffer = []
    outputBytes = 0

def sync_mpsse_interface(dev):
    global outputBuffer
    outputBuffer = [0xAB]
    ft_write_buffer(dev)

def config_mpsse_for_i2c(dev):
    global outputBuffer
    #Disable clock divide by 5, disable adaptive clocking and ENABLE 3 phase
    #data clock
    outputBuffer = [0x8A, 0x97, 0x8c]
    ft_write_buffer(dev)
    #Enable drive-zero mode on the lines used for I2C (0b00000111)
    outputBuffer = [0x9E, 0x07, 0x00]
    ft_write_buffer(dev)
    #Disable internal loopback
    outputBuffer = [0x85]
    ft_write_buffer(dev)

def q_I2C_start_condition():
    global outputBuffer

    #Start condition (SDAO low)
    for i in range(0, 5):
        outputBuffer += [0x80, SCL, SDAO + SCL]

    #Start condition 2 (SDA low, SCL low)
    for i in range(0, 5):
        outputBuffer += [0x80, 0, SDAO + SCL]

def q_I2C_stop_condition():
    global outputBuffer

    #SDA low, SCL should be low alread->kept that way
    for i in range(0, 5):
        outputBuffer += [0x80, 0, SDAO + SCL]

    #Stop condition 2: SCL goes high, data low
    for i in range(0, 5):
        outputBuffer += [0x80, SCL, SDAO + SCL]

    #Stop condition 3: SDA goes high, data low
    for i in range(0, 5):
        outputBuffer += [0x80, SDAO + SCL, SDAO + SCL]

    #Leave bus alone (all pins as inputs)
    q_set_bus_idle()

def q_I2C_sendByte_checkACK(byte):
    global outputBuffer
    #send one byte on the falling edge of SCL
    outputBuffer += [MSB_FALLING_EDGE_CLOCK_BYTE_OUT, 0x00, 0x00, byte]
    #set SCL low + SDA high and disable SDAO to enable the slave to write
    outputBuffer += [0x80, SDAO, SDAO+SCL]
    #get ACK bit on rising edge of SCL
    outputBuffer += [MSB_RISING_EDGE_CLOCK_BIT_IN, 0x00]
    #command to get answer immediately
    outputBuffer += [0x87]

def q_I2C_sendAddr_checkACK(addr, read=False):
    byte = (addr << 1) & 0xFE
    if read:
        byte = byte | 0x01
    q_I2C_sendByte_checkACK(byte)

def q_I2C_readByte_sendNAK():
    global outputBuffer
    outputBuffer += [MSB_RISING_EDGE_CLOCK_BYTE_IN, 0x00, 0x00] #Read one byte on rising edge
    outputBuffer += [MSB_FALLING_EDGE_CLOCK_BIT_OUT, 0x00, 0xFF] #Send one "1"
    q_set_bus_idle()
    outputBuffer += [0x87] #Send answer immediately

#Set SDA+SCL high as the bus idle state.  Bit 2 is as input (data in)
def set_bus_idle(dev):
    dev.write(bytes([0x80, SDAO + SCL, SDAO + SCL]))

def q_set_bus_idle():
    global outputBuffer
    outputBuffer += [0x80, SDAO + SCL, SDAO + SCL]

if __name__ == "__main__":
    dev = ft_open(0)
    dev.resetDevice()
    dev.setUSBParameters(65536,65536)
    dev.setChars(False, 0, False, 0)
    dev.setLatencyTimer(16)
    dev.setBitMode(0x00, 0x00)
    #Bus should be idle before setting the MPSSE mode.  Otherwise, glitches
    #might occur.
    set_bus_idle(dev)
    dev.setBitMode(SDAO + SCL, 0x2)         # Set MPSSE mode
    config_mpsse_for_i2c(dev)
    sync_mpsse_interface(dev)

    set_clock_speed(dev, 10000)          # Set SPI clock to 10000Hz (so that my crap oscilloscope can pick it
                                         # up)
    if dev:
        print("FTDI device opened")
        for i in range(0, 10):
            q_I2C_start_condition()
            q_I2C_sendAddr_checkACK(0x0D, False)
            q_I2C_sendByte_checkACK(0xFF)
            q_I2C_stop_condition()

            ft_write_buffer(dev)
            time.sleep(0.5)
            print("Addr sent")

        dev.close() 