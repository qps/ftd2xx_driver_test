import sys
import time
import ftd2xx

class ftd2xx_uart(object):
    #Opens the device and initialices the required variables. SPI_Mode change not yet implemented
    def __init__(self, baud_rate, uart_mode = "8N1", timeouts = 100, n=0):
        self.outputBuffer = []
        self.outputBytes = 0
        self.inputBuffer = []
        self.inputBytes = 0

        self.__TX = 0x01
        self.__RX = 0x02

        self.d = ftd2xx.open(n)
        self.d.resetDevice()
        time.sleep(0.1)

        self.d.setTimeouts(timeouts, timeouts)
        self.d.setLatencyTimer(16)

        #UART configuration (9600 baud 8N1 without flow control)
        self.d.setBaudRate(baud_rate)
        self.d.purge(0x03)
        self.d.setDataCharacteristics(8, 0, 0)
        self.d.setFlowControl(0, 0, 0)

    def write(self, byte):
        self.d.write(bytes(byte))

    def byte2list(self, byte):
        byte_list = [(byte&(2**i))>>i for i in range(0, 8)]
        return [0] + byte_list + [1]

if __name__ == "__main__":
    uart_dev = ftd2xx_uart(9600)
    print("FTDI device opened")

    for i in range(0, 128):
        data = i
        uart_dev.write([i])
        time.sleep(0.1)
        print("Counter sent: " + str(hex(i)))
        print("Answer received: " + str(uart_dev.d.read(1).hex()))

        #for i in range(0, 100):
        #    q_SPI_CSEnable()
        #    data = i//256,  i%256     #data to write
        #    q_write_data(dev, MSB_FALLING_EDGE_CLOCK_DATA_OUT, data)
        #    q_SPI_CSDisable()
        #    ft_write_buffer(dev)
        #    time.sleep(0.01)
        #    print("Counter sent" + str(data))
        #    q_SPI_CSEnable()
        #    q_read_data(dev, MSB_FALLING_EDGE_CLOCK_DATA_IN, 2)
        #    q_SPI_CSDisable()
        #    ft_write_buffer(dev)
        #    print("Answer received: " + str(dev.read(2).hex()))
        #    time.sleep(0.01)

    uart_dev.d.close()