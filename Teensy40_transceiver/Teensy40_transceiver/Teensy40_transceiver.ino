#include <DMAChannel.h>
#include <SPI.h>
#include "src\t4_i2c\i2c_driver_wire.h"

#define PRREG(x) Serial.print(#x" 0x"); Serial.println(x, BIN) //Serial.println(x,BIN)
#define SAMPLES 1
#define ChipSelectSlave 10

DMAMEM static uint16_t rx_buffer[SAMPLES];
DMAChannel rx(false);

const uint8_t slave_address = 0x0D;

// Double receive buffers to hold data from master.
const size_t slave_rx_buffer_size = 4;
uint8_t slave_rx_buffer[slave_rx_buffer_size] = {};
uint8_t slave_rx_buffer_2[slave_rx_buffer_size] = {};
volatile size_t slave_bytes_received = 0; 
volatile bool i2c_isr_triggered = false;

uint16_t counter = 0x00;
uint8_t outputBuffer[2];

void rxISR() {
    counter++;
    rx.clearInterrupt();
    asm volatile ("dsb");
    Serial.print("RX Interrupt --> Val = "); Serial.print(rx_buffer[0], HEX); Serial.print("  RX counter: "); Serial.println(counter, HEX);
    LPSPI4_TDR = counter;
}

bool initSPISlaveDMA() {
    rx.begin(true);
    rx.source((uint16_t&)LPSPI4_RDR);
    rx.triggerAtHardwareEvent(DMAMUX_SOURCE_LPSPI4_RX);
    rx.attachInterrupt(rxISR);
    rx.interruptAtCompletion(); //TCD->CSR |= DMA_TCD_CSR_INTMAJOR;
    rx.destinationBuffer(rx_buffer, SAMPLES + 1);
    rx.enable();
    return 1;
}

bool initSPISlave() {
    LPSPI4_CR &= ~LPSPI_CR_MEN; //Modul ausschalten
    LPSPI4_CR = LPSPI_CR_RST; //Master Logic reset! (Control Register => Software Reset)
    LPSPI4_CR &= ~LPSPI_CR_RST; //Master Logic reset! (Control Register => Software Reset)
    LPSPI4_DER = LPSPI_DER_RDDE; //RX DMA Request Enable
    LPSPI4_CR |= LPSPI_CR_MEN; //Enable SPI Module!

    LPSPI4_TCR &= ~LPSPI_TCR_CPOL;
    LPSPI4_TCR &= ~LPSPI_TCR_CPHA;
    LPSPI4_TCR &= ~LPSPI_TCR_LSBF;
    LPSPI4_TCR &= ~LPSPI_TCR_WIDTH(0b00);
    LPSPI4_TCR = LPSPI_TCR_FRAMESZ(15); //16Bit Mode
    LPSPI4_TDR = 0x0000; //Reset the TDR
    return 1;
}

void setup() {
    Serial.begin(115200);
    SPI.begin();
    pinMode(ChipSelectSlave, INPUT);
    SPI.setCS(ChipSelectSlave);

    Wire.begin(slave_address);
    Wire.onReceive(i2c_isr);

    while (!Serial);

    Serial.println("Init SPI!");
    if (initSPISlave()) {
        Serial.println("SPI SLAVE init!");
    }
    if (initSPISlaveDMA()) {
        Serial.println("DMA Channel init!");
    }
}

void loop() {
    arm_dcache_delete(rx_buffer, SAMPLES); //delete Cache!
    delay(100);
}

void i2c_isr(int howMany) {
    int x = Wire.read();           // receive byte as an integer
    Serial.println(x);             // print the integer
}
